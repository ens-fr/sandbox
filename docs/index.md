# Tests

## Maths

$$\mathrm e^{\mathrm i \pi} + 1 = 0$$

## MkDocs

!!! info "Boîte"
    === "volet 1"
        contenu 1

    === "volet 2"
        contenu 2

## Thumbnails

<https://pypi.org/project/mkdocs-thumbnails/>

[Documentation d'asymptote](asymptote.pdf){.pdf}  
[Bonne zik](https://www.youtu.be/UYpMLqXR6cw){.youtube}

## Jupyter

<https://pypi.org/project/mkdocs-jupyter/>



## Macros

{{ macros_info() }}
