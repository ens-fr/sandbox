# Basthon

## Console

Un exemple d'exercice où l'élève peut s'évaluer seul.

{{ python_ide('fermat.py') }}

## Carnet

Un exemple de carnet avec importation d'un module un peu caché qui teste la ou les fonctions de l'élève.

{{ python_carnet('test.ipynb', module='juge.py') }}
