import math;
import markers;

size(6cm, 0);

// Définition de styles de points
marker croix = marker(scale(3)*cross(4),
                    1bp+gray);

add(grid(6, 6, .8lightgray));

pair pA = (1, 1), pB = (5, 5), pC = (2, 4);

draw(pA--pB--pC--cycle);
draw("$A$", pA, SW,blue, croix);  
draw("$B$", pB, SE,blue, croix);  
draw("$C$", pC, NW, croix);

draw(pA--pC, StickIntervalMarker(1, 2, size=8, angle=-45, red));
draw(pB--pC, StickIntervalMarker(1, 2, size=8, angle=-45, red));
