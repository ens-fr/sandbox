// Un exemple pour expliquer qu'il y a deux
// types possibles pour définir des points :

// - le type pair (défini nativement dans asymptote) est 
//   un couple de réels dans le repère par défaut ;
// - le type point (défini dans l'extension geometry)
//   est un couple de réels relatif à un repère choisi.

import geometry;
size(7cm,0);
// Le repère courant est currentcoordsys 
// et sa valeur par défaut est defaultcoordsys.
show(defaultcoordsys);

pair  pA=(1,.5);
point pB=(.5,1); 

dot("$A$",pA,N,5bp+.5red);
dot("$B$",pB,N,5bp+.5blue);

// On change de répertoire courant et on l'affiche.
currentcoordsys=cartesiansystem((2,1),i=(1,1),j=(-1,1));
show("$O'$","$\vec{u}$","$\vec{v}$",currentcoordsys);

// On redéfinit un "pair" et un "point" avec les mêmes coordonnées.
pair  pAp=(1,.5);
point pBp=(.5,1); 

dot("$A'$",pAp,S,3bp+red);
dot("$B'$",pBp,S,3bp+blue);
