# Inclure du matériel externe

## GnuPlot

!!! note "Contenu de `trigo.dat`"
    ```
    --8<--- "docs/gnuplot/trigo.dat"
    ```

!!! done "Rendu"
    {{ gnuplot('trigo.dat') }}

## Asymptote

!!! note "Contenu de `figure_1.asy`"
    ```
    --8<--- "docs/asy/figure_1.asy"
    ```

!!! done "Rendu"
    {{ asy('figure_1.asy') }}


## Asymptote 3D WebGL

On peut la tourner en 3D et zoomer en gardant du vectoriel.

{ asy3D('theapot.asy') }

{{ info() }}

Vieille version de asymptote !!!

<iframe src="../theapot.html" width="568" height="416" frameborder="0"></iframe>

[En plein écran, c'est magique !!! Pot](../theapot.html)
