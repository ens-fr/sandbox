# Autres tests

## Print site en pdf

<https://timvink.github.io/mkdocs-print-site-plugin/index.html>

## Asciinema + audio

<https://github.com/dhobsd/castty>

## Thème 386

<https://lramage.gitlab.io/mkdocs-bootstrap386/>

