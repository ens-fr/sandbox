# Tests Graphiques

## Asymptote

!!! tip "truc"
    Alors

    ```asy
    import math;
    import markers;

    size(6cm, 0);

    // Définition de styles de points
    marker croix = marker(scale(3)*cross(4),
                        1bp+gray);

    add(grid(6, 6, .8lightgray));

    pair pA = (1, 1), pB = (5, 5), pC = (2, 4);

    draw(pA--pB--pC--cycle);
    draw("$A$", pA, SW,blue, croix);  
    draw("$B$", pB, SE,blue, croix);  
    draw("$C$", pC, NW, croix);

    draw(pA--pC, StickIntervalMarker(1, 2, size=8, angle=-45, red));
    draw(pB--pC, StickIntervalMarker(1, 2, size=8, angle=-45, red));
    label(pA, "Figure 1 - un bug est ici !!!", E);
    ```

    Figure 1 : un triangle $ABC$.


```asy
// Un exemple pour expliquer qu'il y a deux
// types possibles pour définir des points :

// - le type pair (défini nativement dans asymptote) est 
//   un couple de réels dans le repère par défaut ;
// - le type point (défini dans l'extension geometry)
//   est un couple de réels relatif à un repère choisi.

import geometry;
size(7cm,0);
// Le repère courant est currentcoordsys 
// et sa valeur par défaut est defaultcoordsys.
show(defaultcoordsys);

pair  pA=(1,.5);
point pB=(.5,1); 

dot("$A$",pA,N,5bp+.5red);
dot("$B$",pB,N,5bp+.5blue);

// On change de répertoire courant et on l'affiche.
currentcoordsys=cartesiansystem((2,1),i=(1,1),j=(-1,1));
show("$O'$","$\vec{u}$","$\vec{v}$",currentcoordsys);

// On redéfinit un "pair" et un "point" avec les mêmes coordonnées.
pair  pAp=(1,.5);
point pBp=(.5,1); 

dot("$A'$",pAp,S,3bp+red);
dot("$B'$",pBp,S,3bp+blue);
```

Figure 2 : un repère avec deux vecteurs.

:+1: Ça marche pas mal :smiley: ; même dans une admonition.

:warning: Il faudrait un répertoire temporaire et
 des noms de fichiers uniques, pour pouvoir générer plusieurs figures.

:warning: l'ordre des figures n'est pas respecté.

:warning: Ne fonctionne pas dans une balise ````` ````asy ... ```` `````

: warning Ne fonctionne pas dans une admonition