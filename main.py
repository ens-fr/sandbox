import os
import sys
import subprocess
import shutil

def define_env(env):
    "Hook function"

    @env.macro
    def info() -> str:
        sortie = ""

        processus_complet = subprocess.run(
            ["gnuplot", "--version"],
            capture_output=True,
            text=True)
        if processus_complet.returncode != 0:
            return "Erreur avec GnuPlot\n" + processus_complet.stderr
        sortie += processus_complet.stdout
        sortie += processus_complet.stderr
        
        sortie += "\n"*2

        processus_complet = subprocess.run(
            ["asy", "-version"],
            capture_output=True,
            text=True)
        if processus_complet.returncode != 0:
            return "Erreur avec Asy\n" + processus_complet.stderr
        sortie += processus_complet.stdout
        sortie += processus_complet.stderr

        return sortie


    @env.macro
    def run(moteur: str, script: str, args_moteur=[]) -> str:
        """macro qui prend
            - un moteur [python, gnuplot, asy, latex, ...]
            - un nom de fichier `script` qui est dans {env.variables.script_dir}
        - déduit une association de sortie (type + affichage)
            - matplotlib | gnuplot | asy | latex -> .svg -> dans une balise image
            - python -> .out -> dans une fence python
            - drawSvg | ipythonblock -> .html -> nature
        
        >>> run('python', 'truc.py')
        '```python\nsortie .txt de truc.py\n```\n'
        
        >>> run('gnuplot', 'fig.p')
        '![gnuplot fig.p](fig.p.svg)'
        
        >>> run('drawSvg', 'bidule.py')
        'du contenu .html prêt à tourner'
        
        >>> run('python-moteur2', 'jouet.py')  # 'moteur2' pourrait être 'mermaid' | 'raw' | '' |
        '```moteur2\nsortie de truc.py\n```\n'
        
        
        - La macro regarde si `script.sortie.svg|.html|.txt` existe aussi et est plus récent,
        - sinon, regarde les droits en écriture dans le dossier de `script`
        - crée ou recrée `...sortie...` dans le dossier de `script`
        - ou alors un nouveau dossier local `tmp_run_macro`.

        """
        try:
            m_time_src = os.stat(env.variables.gnuplot_dir + file).st_mtime_ns
        except FileNotFoundError:
            print(f"WARNING macro gnuplot : Fichier {file} absent", file=sys.stderr)
            return f":warning: Fichier {file} absent"

        found = True
        try:
            m_time_svg = os.stat(env.variables.gnuplot_dir + file + '.svg').st_mtime_ns
        except FileNotFoundError:
            found = False

        if not(found) or (m_time_src > m_time_svg):
            pass
        # pas finie
        
    @env.macro
    def gnuplot(file: str) -> str:
        processus_complet = subprocess.run(
            ["gnuplot", env.variables.gnuplot_dir + file],
            capture_output=True,
            text=True)
        if processus_complet.returncode != 0:
            return "Erreur avec GnuPlot\n" + processus_complet.stderr
        sortie = processus_complet.stdout.replace('\n', '\t')
        return sortie[sortie.find('<svg'):]

    @env.macro
    def asy(file: str) -> str:
        racine, suffix = file[:-4], file[-4:]
        if suffix != '.asy':
            return f"Fichier {file} invalide ; il doit se terminer par '.asy'"
        
        tmp_eps = "temp_" + racine + ".eps"
        processus_complet = subprocess.run(
            ["asy", "-o" + tmp_eps, 
             env.variables.asy_dir + file],
            capture_output=True)
        if processus_complet.returncode != 0:
            return "Erreur avec Asy\n" + processus_complet.stderr.decode("utf-8")
        
        processus_complet = subprocess.run(
            ["epstopdf", tmp_eps],
            capture_output=True)
        if processus_complet.returncode != 0:
            return "Erreur avec epstopdf\n" + processus_complet.stderr.decode("utf-8")
        
        tmp_pdf = "temp_" + racine + ".pdf"
        tmp_svg = "temp_" + racine + ".svg"
        processus_complet = subprocess.run(
            ["pdf2svg", tmp_pdf, tmp_svg],
            capture_output=True)
        if processus_complet.returncode != 0:
            return "Erreur avec pdf2svg\n" + processus_complet.stderr.decode("utf-8") + \
                processus_complet.stdout.decode("utf-8") + "\n" + \
                    "\n".join(truc for truc in processus_complet.args)
        
        with open(tmp_svg, 'r') as f:
            sortie = f.readlines()

        # ménage
        processus_complet = subprocess.run(
            ["rm", tmp_eps, tmp_pdf, tmp_svg],
            capture_output=True)
        if processus_complet.returncode != 0:
            return "Erreur avec rm\n" + processus_complet.stderr.decode("utf-8") + \
                processus_complet.stdout.decode("utf-8") + "\n" + \
                    "\n".join(truc for truc in processus_complet.args)
        
        return "\t".join(sortie[1:])

    @env.macro
    def asy3D(file: str) -> str:
        racine, suffix = file[:-4], file[-4:]
        if suffix != '.asy':
            return f"Fichier {file} invalide ; il doit se terminer par '.asy'"

        sortie_html = racine + ".html"
        processus_complet = subprocess.run(
            ["asy","-fhtml", "-offline", "-odocs/"+sortie_html,
             env.variables.asy_dir + file],
            capture_output=True)
        if processus_complet.returncode != 0:
            return "Erreur avec Asy\n" + processus_complet.stderr.decode("utf-8")
        
        return f'<iframe src="../{sortie_html}" width="568" height="416" frameborder="0"></iframe>'



    @env.macro
    def python_ide(exo: str, hauteur: int = 700) -> str:
        """Renvoie du HTML pour embarquer un fichier `exo` dans un ide sommaire
        + Basthon est la solution 2021, RGPD ok
        """
        lien = f"https://console.basthon.fr/?from={env.variables.scripts_url}{exo}"
        return f"<iframe src={lien} width=100% height={hauteur}></iframe>" + \
                f"[Lien dans une autre page]({lien})"
    
    @env.macro
    def python_carnet(carnet: str, aux: str = '', module: str = '',
                      auxs=None, modules=None,
                      hauteur: int = 700) -> str:
        """Renvoie du HTML pour embarquer un fichier `carnet.ipynb` dans un notebook
        + Basthon est la solution 2021, RGPD ok
        """
        lien = f"https://notebook.basthon.fr/?"
        if carnet != '':
            lien += f"from={env.variables.scripts_url}{carnet}&"
        else:
            lien += f"from={env.variables.scripts_url}1_cell_python.ipynb&"
        
        if aux != '':
            lien += f"aux={env.variables.scripts_url}{aux}&"
        if auxs is not None:
            for aux in auxs:
                lien += f"aux={env.variables.scripts_url}{aux}&"
        
        if module != '':
            lien += f"module={env.variables.scripts_url}{module}&"
        if modules is not None:
            for module in modules:
                lien += f"module={env.variables.scripts_url}{module}&"
        
        return f"<iframe src={lien} width=100% height={hauteur}></iframe>" + \
                f"[Lien dans une autre page]({lien})"
